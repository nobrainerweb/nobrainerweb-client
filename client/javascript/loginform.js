document.addEventListener('DOMContentLoaded', () => {
	addBg();
});

/**
 * add div to body
 */
const addBg = () => {
	const body = document.querySelector('body');
	const div = document.createElement('div');
	//const imgUrl = '/resources/vendor/nobrainerweb/silverstripe-client/client/images/velizar-ivanov-558323-unsplash.jpg';
	const imgUrl = 'https://api.nobrainer.dk/login-api/image';
	div.classList.add('security-img-bg');
	div.style.backgroundImage = `url(${imgUrl})`;

	body.append(div);
};