import gulp from 'gulp';
import plugins from 'gulp-load-plugins';

import browserify from 'browserify';
import babelify from 'babelify';

import source from 'vinyl-source-stream';
import buffer from 'vinyl-buffer';

import autoprefixer from 'autoprefixer';
import log from 'fancy-log';
import yargs from 'yargs';

const $ = plugins();
const args = yargs.argv;

const LIVE = args.live || args.l;

const sources = {
	js: 'javascript/loginform.js',
	scss: ['scss/loginform.scss']
};

const globs = {
	js: 'javascript/**/*.js',
	scss: 'scss/**/*.scss'
};


// Dynamic source function generators

function lint(src) {
	return function linting() {
		return gulp.src(src)
			.pipe($.eslint())
			.pipe($.eslint.format())
			.pipe($.eslint.failAfterError());
	};
}

function babel(src) {
	const name = Array.isArray(src) ? 'loginform.js' : src.replace(/^.*\//, '');
	return function building() {
		return browserify({entries: src}).transform(babelify).bundle()
			.pipe(source(name))
			.pipe(buffer())
			.pipe($.sourcemaps.init())
			.pipe($.if(LIVE, $.uglify().on('error', log.error)))
			.pipe($.sourcemaps.write('.'))
			.pipe(gulp.dest('build/'));
	};
}

function sass(src) {
	const plugins = [autoprefixer({browsers: ['last 4 versions', 'ie >= 10', 'and_chr >= 2.3']})];
	return function sassing() {
		return gulp.src(src)
			.pipe($.sourcemaps.init())
			.pipe($.sass().on('error', $.sass.logError))
			.pipe($.postcss(plugins))
			.pipe($.if(LIVE, $.cssmin()))
			.pipe($.sourcemaps.write('.'))
			.pipe(gulp.dest('build/'));
	};
}


// Hardcoded source functions

function fonts() {
	return gulp.src(['node_modules/font-awesome/fonts/**/*', 'node_modules/lightgallery/src/fonts/**/*'])
		.pipe(gulp.dest('fonts/modules'));
}

function images() {
	return gulp.src('node_modules/lightgallery/src/img/**/*')
		.pipe(gulp.dest('images/modules'));
}


// Gulp tasks

gulp.task('js',
	gulp.parallel(
		babel(sources.js)
	)
);

gulp.task('lint', lint(sources.js));

gulp.task('sass', sass(sources.scss));

gulp.task('node', gulp.parallel(fonts, images));

gulp.task('build',
	gulp.parallel('node', 'sass',
		gulp.series('lint', 'js')
	)
);

gulp.task('watch',
	gulp.series('build', function watching() {
		// Watch js sources
		gulp.watch(globs.js).on('change',
			gulp.series(lint(sources.js), babel(sources.js)));
		// Watch scss sources.
		gulp.watch(globs.scss).on('change', gulp.series(sass(sources.scss)));
	})
);

gulp.task('default', gulp.series('watch'));