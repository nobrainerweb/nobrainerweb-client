<?php
/**
 * Created by PhpStorm.
 * User: sanderhagenaars
 * Date: 12/08/2018
 * Time: 22.15
 */

namespace Nobrainerweb\Client\OAuth2\Helpers;

use SilverStripe\Control\Controller;
use SilverStripe\Core\Config\Configurable;

class Helper
{
    use Configurable;

    /**
     * @config string
     */
    private static $url = 'https://api.nobrainer.dk';

    /**
     * @config string
     */
    private static $testUrl = 'http://dev.api.nobrainer.dk';

    /**
     * @config bool
     */
    private static $testMode = false;

    /**
     * enable oauth check
     *
     * @config bool
     */
    private static $enableAdminOauth = true;

    /**
     * @config string
     */
    private static $clientId;

    /**
     * @config string
     */
    private static $clientSecret;

    /**
     * Holders usernames that should be checked agains oauth server
     *
     * @config array
     */
    private static $usernames = [];

    /**
     * @return array
     */
    public static function getClientCredentials(): array
    {
        $config = self::config();

        $credentials = [
            'clientId'     => $config->get('clientId'),
            'clientSecret' => $config->get('clientSecret')
        ];

        return $credentials;
    }

    /**
     * Return the usernames that will trigger oauth validation
     *
     * @return array
     */
    public static function getSpecialOAuthUsers(): array
    {
        return self::config()->get('usernames');
    }

    /**
     * @return bool
     */
    public static function getTestMode(): bool
    {
        return self::config()->get('testMode');
    }

    /**
     * @return null|string
     */
    public static function getCustomTestUrl(): ?string
    {
        return self::config()->get('testUrl') ?? null;
    }

    /**
     * @return bool
     */
    public static function oauthEnabled(): bool
    {
        return self::config()->get('enableAdminOauth');
    }

    /**
     * @return string
     */
    public static function getBaseUrl(): string
    {
        return self::getTestMode() ? self::getCustomTestUrl() : self::config()->get('url');
    }

    /**
     * @return string
     */
    public static function getBaseAuthorizationUrl(): string
    {
        return Controller::join_links(self::getBaseUrl(), 'oauth2', 'authorise');
    }
}