<?php

namespace Nobrainerweb\Client\OAuth2\Authenticator;

use Nobrainerweb\Client\OAuth2\Controllers\Controller;
use Nobrainerweb\Client\OAuth2\Helpers\Helper;
use SilverStripe\ORM\ValidationResult;
use SilverStripe\Security\Member;

/**
 * Created by PhpStorm.
 * User: sanderhagenaars
 * Date: 04/10/2018
 * Time: 08.51
 */
class MemberAuthenticator extends \SilverStripe\Security\MemberAuthenticator\MemberAuthenticator
{
    /**
     * Check if username should be held up against oauth, else just default
     *
     * @param Member                $member
     * @param string                $password
     * @param ValidationResult|null $result
     * @return ValidationResult
     */
    public function checkPassword(Member $member, $password, ValidationResult &$result = null)
    {
        $users = Helper::getSpecialOAuthUsers();
        $identifier = Member::singleton()->config()->get('unique_identifier_field');
        $enabled = Helper::oauthEnabled();

        // if username is not set up for oauth, return regular password check
        if (!\in_array($member->{$identifier}, $users, true) || !$enabled) {
            return parent::checkPassword($member, $password, $result);
        }

        // call server
        $controller = Controller::create();
        // call oauth server and check with credentials
        $response = $controller->authenticate(['Email' => $member->{$identifier}, 'Password' => $password]);

        if (!$result) {
            $result = ValidationResult::create();
        }

        if ((int)$response->getStatusCode() !== 200) {
            $result->addError(_t(
                'SilverStripe\\Security\\Member.ERRORWRONGCRED',
                "The provided details don't seem to be correct. Please try again."
            ), 'bad');
        }

        return $result;
    }
}