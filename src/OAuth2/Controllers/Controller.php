<?php
/**
 * Created by PhpStorm.
 * User: sanderhagenaars
 * Date: 12/08/2018
 * Time: 01.59
 */

namespace Nobrainerweb\Client\OAuth2\Controllers;


use GuzzleHttp\Client;
use Nobrainerweb\Client\OAuth2\Helpers\Helper;
use SilverStripe\Control\HTTPResponse;
use SilverStripe\Security\Member;

class Controller extends \SilverStripe\Control\Controller
{
    private static $allowed_actions = [
        'authenticate'
    ];

    /**
     * get user authorised by oauth server
     *
     * @param array $data
     * @return HTTPResponse
     */
    public function authenticate(array $data): HTTPResponse
    {
        $identifierField = Member::singleton()->config()->get('unique_identifier_field');
        $username = $data[$identifierField];
        $password = $data['Password'];

        $credentials = Helper::getClientCredentials();

        $guzzle = new Client;

        $statusCode = 500;
        try {
            $response = $guzzle->request('POST', Helper::getBaseAuthorizationUrl(), [
                'headers'     => ['Content-Type' => 'application/x-www-form-urlencoded', 'Accept' => 'application/json'],
                'form_params' => [
                    'grant_type'    => 'password',
                    'client_id'     => $credentials['clientId'],
                    'client_secret' => $credentials['clientSecret'],
                    'username'      => $username,
                    'password'      => $password,
                    'scope'         => ''
                ]
            ]);
            $statusCode = $response->getStatusCode();
            $body = $response->getBody()->getContents();
        } catch (\Exception $e) {
            $body = $e->getMessage();
        }

        $ssResponse = HTTPResponse::create();
        $ssResponse->setStatusCode($statusCode);
        $ssResponse->setBody($body);

        return $ssResponse;
    }
}