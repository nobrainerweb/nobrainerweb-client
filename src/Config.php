<?php
/**
 * Created by PhpStorm.
 * User: sanderhagenaars
 * Date: 16/09/2016
 * Time: 14:02
 */

namespace NobrainerWeb\Client;


use SilverStripe\Core\Config\Config_ForClass;
use SilverStripe\ORM\DataExtension;
use SilverStripe\SiteConfig\SiteConfig;

class Config extends DataExtension
{

	/**
	 * @config string
	 */
	private static $db_prefix;

	/**
	 * @config string
	 */
	private static $api_key;

	/**
	 * Helper for getting static shop config.
	 * The 'config' static function isn't avaialbe on Extensions.
	 *
	 * @return Config_ForClass configuration object
	 */
	public static function config()
	{
		return new Config_ForClass('NobrainerWebClient\Config');
	}

	public static function current()
	{
		return SiteConfig::current_site_config();
	}

	/**
	 * @return string
	 */
	public function getDBprefix()
	{
		return $this->config()->db_prefix;
	}

	/**
	 * @return string
	 */
	public function getApiKey()
	{
		return $this->config()->api_key;
	}
}