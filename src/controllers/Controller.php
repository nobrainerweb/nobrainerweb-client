<?php
/**
 * Created by PhpStorm.
 * User: sanderhagenaars
 * Date: 05/04/2018
 * Time: 11.05
 */

namespace Nobrainerweb\Client\Controllers;


use NobrainerWeb\Client\Config;
use SilverStripe\Admin\LeftAndMain;
use SilverStripe\Control\Director;

class Controller extends \SilverStripe\Control\Controller
{

    private static $allowed_actions = array(
        'siteinfo'
    );

    /**
     * Check the API key on every call
     */
    public function init()
    {
        parent::init();

        $received_api_key = $this->getRequest()->param('Apikey');

        if(!$received_api_key){
            $response = $this->getResponse();
            $response->setBody(json_encode(['errors' => ['code' => '403', 'message' => 'No api token']]));
            $response->setStatusCode(403);

            return $response;
        }

        $apikey = Config::current()->getApiKey();

        if ($apikey != $received_api_key) {
            //user_error('You are not allowed to perform this action');

            $return_data = array();
            $return_data['errors'] = array(
                'code' => 401,
                'message' => 'Invalid API token'
            );

            $response = $this->getResponse();
            $response->setBody(json_encode($return_data));
            $response->setStatusCode(401);

            return $response;
        }
    }

    /**
     * returns info about the site in JSON format
     */
    public function siteinfo()
    {
        $response = $this->getResponse();


        $return_data = array();
        $return_data['version'] = self::siteversion();
        $return_data['mode'] = self::sitemode();

        $response->setBody(json_encode($return_data));

        return $response;
    }

    public function siteversion()
    {
        return LeftAndMain::singleton()->CMSVersion();

    }

    public function sitemode()
    {
        return Director::get_environment_type();
    }

}