<?php


namespace NobrainerWeb\Client\Security;

use SilverStripe\ORM\DataExtension;
use SilverStripe\View\Requirements;

class SecurityExtension extends DataExtension
{
    public function onAfterInit(): void
    {
        Requirements::css('nobrainerweb/silverstripe-client:client/build/loginform.css');
        Requirements::javascript('nobrainerweb/silverstripe-client:client/build/loginform.js');
    }
}